<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Config;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $con = Config::connect();
        Schema::connection($con)->create('books', function (Blueprint $table) {
            $table->id();
            $table->string('name')->unique();
            $table->foreignId('category_id')
            ->nullable()
            ->constrained('categories')
            ->onDelete('cascade');
            $table->string('writer');
            $table->string('price');
            $table->year('GeorgianYear');
            $table->year('SolarYear');
            $table->string('image');
            $table->integer('viewCount');
            $table->integer('likeCount');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}

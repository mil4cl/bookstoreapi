<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User as User;
use App\Http\Controllers\Traits\ImageUpload;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    use ImageUpload;
    /**
     * Store a new user.
     *
     * @param  Request  $request
     * @return Response
     */
    public function register(Request $request)
    {
        //validate incoming request
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|min:3|max:255',
            'lastname' => 'required|string|min:3|max:255',
            'email' => 'required|email|unique:users',
            'gender' => 'required|in:male,female',
            'age' => 'required|Numeric',
            'avatar' => 'nullable|mimes:jpeg,jpg,png,gif|max:100000',
            'birth_date' => 'required|date_format:Y-m-d',
            'register_at' => 'required|date_format:Y-m-d H:i:s',
            'login_at' => 'required|date_format:Y-m-d H:i:s',
            'password' => 'required|confirmed',
        ]);
        $data = $validator->validated();

        try {
            $user = User::create([
                'name' => $data['name'],
                'lastname' => $data['lastname'],
                'email' => $data['email'],
                'gender' => $data['gender'],
                'age' => $data['age'],
                'avatar' => $data['avatar'],
                'birth_date' => $data['birth_date'],
                'register_at' => $data['register_at'],
                'login_at' => $data['login_at'],
                'password' => app('hash')->make($data['password'])
            ]);
            $this->saveImages($data['avatar']);
            $user->save();

            //return successful response
            return response()->json(['user' => $user, 'message' => 'CREATED'], 201);
        } catch (\Exception $e) {
            //return error message
            return response()->json(['message' => 'User Registration Failed!'], 409);
        }
    }
}

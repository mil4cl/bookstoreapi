<?php
namespace App\Http\Controllers\Traits;
use App\Libraries\Helpers;

trait ImageUpload
{

    /**
     * Image upload trait used in controllers to upload files
     */
    public function saveImages($file)
    {
        $destinationPath = '/uploads/';

        $file_name = time().'-'.$file->getClientOriginalName();

        //$image = Image::make($file);

        $file->resize(1920, 1080, function ($constraint) {
              $constraint->aspectRatio();
        })->save(Helpers::public_path() . $destinationPath . $file_name);

        $file->resize(350, 240, function ($constraint) {
              $constraint->aspectRatio();
        })->save(Helpers::public_path() . $destinationPath .'/thumbnails/'. $file_name);

        return $file_name;
    }

}

<?php

namespace App;

use App\Models\Connection;
use Illuminate\Support\Facades\Schema;

class Config
{
    public static function connect()
    {
        if (Schema::connection('mysql')->hasTable('connections')) {
            $con = Connection::on('mysql')->whereName('pgsql')->first();
            if ($con != null) {
                return $con->name;
            }
        }
        return "mysql";
    }
}

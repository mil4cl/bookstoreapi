<?php

namespace App\Libraries;


class Helpers
{

    public static function public_path($path = null)
    {
        return rtrim(app()->basePath('public/' . $path), '/');
    }
}

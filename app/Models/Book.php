<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Config;
class Book extends Model
{
    protected $connection;

    public function __construct()
    {
        $this->connection = Config::connect();
    }
}
